# Img Uploader

This is an image uploader using Express.js.

I would not suggest using this for production anything.

If you are a part of the May Cohort and are interested in using this, please PM me on slack for an authorization token.

## Installation

```sh
cp example.env .env
```

You need to edit the env file to suit your needs. The `TOKENS` variable must be comma separated per token and be a valid javascript string. UUIDs should work just fine.

```sh
npm install
node index.mjs
```

## MAY PT Cohort

### Guidelines

- This is only for images, like PNG, JPEG, GIF, etc.

- The file size limit is set to 25MB. The VPS it's running on has very limited storage space, so I'd ask that you be mindful of that when building your applications.

- If you want a key to access the API, message me (Josh Vasquez) on Slack.

- Treat this as temporary file storage. Images will likely be wiped eventually if necessary.

- Use this only for user-generated images (like profile pictures, or other types of images); static images can be deployed with your application.

### Usage

https://images.daisuke.dev itself is an example of how you might use this. It is one self-contained javascript file. Alternative, you can of course just look at the source code inside the `demo/` directory of this repository

Request: Note that this is an `https` href. It will not work with `http`.

```http
POST https://images.daisuke.dev/images HTTP/1.1
Content-Type: multipart/form-data
Authorization: <authToken> - This will probably be a random UUID I assign to you.
```

You will need to attach an `Authorization` header :

```
Authorization: <authToken>
```

It is a multipart form. Your image must be a form field named 'image' and it will only accept a single image. It verifies that it is an image using its mimetype.

If successful, you will receive a JSON response similar to this one:

Response Shape (`JSON`)

```jsonc
// Status: 200 Ok
{
  "message": "Image uploaded successfully.",
  "href": "https://images.daisuke.dev/images/5a369051-5858-4963-81f0-72724f4c6166the-cheat.gif"
}
```

If you supply an incorrect `authToken`:

```jsonc
// Status: 401 Unauthorized
{
  "message": "Unauthorized"
}
```

Other errors will return some form of 400 Response with a message attached:

```jsonc
// Status: 400 Bad Request, 405 Method not Allowed, 413 Request Entity too Large
{
  "message": "File too large"
}
```
