import express from "express";
import multer from "multer";
import { v4 as uuidv4 } from "uuid";
import dotenv from "dotenv";
import cors from "cors";
import path from "path";
import morgan from "morgan";
import fs from "fs";

dotenv.config();
const tokens = process.env.TOKENS.split(",");
const prod =
  process.env.PROD === "true" || process.env.PROD === "TRUE" ? true : false;
const port = process.env.PORT;
const domainPath = process.env.DOMAIN;
const imageSizeLimit = parseInt(process.env.FSLIMIT);
const domain = `${domainPath}${port && !prod ? ":" + port : ""}/`;
const accessLogStream = fs.createWriteStream(path.join("logs/", "access.log"), {
  flags: "a",
});

const isAuthed = (req, res, next) => {
  const authToken = req.headers.authorization;
  console.log("auth token: ", authToken);
  if (tokens.includes(authToken)) {
    return next();
  } else {
    return res.status(401).json({ message: "Unauthorized" });
  }
};

const filter = (req, file, cb) => {
  if (file.mimetype.startsWith("image/")) {
    cb(null, true);
  } else {
    cb(new Error("Only images are allowed."));
  }
};

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "images/");
  },
  filename: (req, file, cb) => {
    cb(null, uuidv4() + file.originalname.replaceAll(" ", "-"));
  },
});
const upload = multer({
  storage: storage,
  fileFilter: filter,
  limits: {
    fileSize: 1024 * 1024 * imageSizeLimit,
  },
});

const app = express();

app.use(cors());
app.use(morgan("combined", { stream: accessLogStream }));
app.use(morgan("combined"));

app.get("/", (req, res) => {
  res.sendFile(`index.html`, { root: path.join("demo/") });
});

app.post("/images", isAuthed, (req, res) => {
    console.log(req.file, req.body);
  upload.single("image")(req, res, (err) => {
    if (err instanceof multer.MulterError) {
      return res.status(405).json({ message: err.message });
    } else if (err) {
      return res.status(500).json({
        message: "An error occurred while processing the image upload.",
      });
    }

    if (!req.file) {
      return res.status(400).json({ message: "Image could not be uploaded." });
    }
    const href = domain + req.file.path;
    res.status(200).json({
      message: "Image uploaded successfully.",
      href: href,
    });
  });
});

app.use("/images", express.static("images/"));

app.listen(port, () => {
  console.log(`App running on port ${domain}`);
});
